<pre><?php

include "../brainfuck.php";

echo brainfuck(
"ASCII character for '9':
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
 ASCII character for ' ' (space):
 >++++++++++++++++++++++++++++++++
 
 Loop counter set to 10
 >++++++++++
 
 Run loop
 [<<.->.>-]"
);

?></pre>